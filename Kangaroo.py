#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the kangaroo function below.

k1 = []
def kangaroo(x1, v1, x2, v2):
    count = 0
    for i in range(0,4):
        k1.append(x1 * i + v1)
        x1 += i

    print(k1)

if __name__ == '__main__':
    x1 = 0
    v1 = 2
    x2 = 3
    v2 = 3
    result = kangaroo(x1, v1, x2, v2)
